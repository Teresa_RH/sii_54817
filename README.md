## Instrucciones de juego
# Elementos
	El juego estará formado por dos plataformas verticales llamadas raquetas y una esfera.
# Dinámica de juego
	Cada jugador será responsable del movimiento de una de las raquetas. 
	La esfera estará flotando entre las raqueta con un movimiento tal que se irá a chocar con las paredes, cuando la esfera se vaya a colisionar con las paredes en las que se encuentran las raquetas, los jugadores tendrán que impedirlo interponiendose ente la esfera y la pared para conseguir puntos. 
	La esfera irá disminuyendo su radio conforme pasa el tiempo de juego.
