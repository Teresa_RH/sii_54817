Aqui se pondran los cambios funcionales explicados en alto nivel

## [1.4]- 2021.12.7
### Added
- Se separa el código del programa tenis en dos programas diferentes denominados cliente y servidor.
- Se comunica cliente y servidor de manera concurrente.

## [1.3]- 2021.10.11
### Added
- Se añade sistema de información compartida para imprimir las puntuaciones
- Se le da autonomía a una de las raquetas del juego para poder jugar contra la máquina(No funciona)

## [1.2]- 2021.10.28
### Added
- Se añade movimiento a las raquetas y esferas del juego 
- Se modifica la esfera para que vaya disminuyendo de tamaño a medida que pasa el tiempo, cuando llega a un tamaño concreto vuelve a su tamaño original.

## [1.1]- 2021.10.07
### Added
 - Se crea el changelog del proyecto
 - Se añade el nombre en los archivos .cpp  

