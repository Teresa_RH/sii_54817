#include <fstream>
#include "glut.h"
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char **argv) {
	int fd;
	//struct stat bstat;
	DatosMemCompartida * DMC;
	fd= open("BOTY", O_RDWR|O_CREAT|O_TRUNC,0777);

	if(fd<0)
	{	
	perror("No puede abrirse el BOTY");
	exit(1);
	}
	
//fstat(fd, &bstat);

	DMC = (DatosMemCompartida*) mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);
	if (DMC==((void *) -1))
	{
	perror("mmap"); 
	exit(1);
	}	
	close(fd);

	while(1){
	usleep(25000);
 	if((DMC->raqueta1.y1)==(DMC->esfera.centro.y))
 		DMC->accion=0;
	else 
	{
 		if((DMC->raqueta1.y1) < (DMC->esfera.centro.y)) 
 			DMC->accion=1;
 		else 
 		{
 		DMC->accion=-1;
 		}
 	}
	}
//munmap(DMC, sizeof(DatosMemCompartida));

	return 0;

}
