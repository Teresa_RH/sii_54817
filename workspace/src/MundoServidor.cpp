// MundoServidor.cpp: implementation of the CMundoServidor class.
//Editado por Teresa Ramirez Haro
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	close(fd_fifo);
	unlink("FIFO");

	/*close(fd_fifo_sc);
  	unlink("FIFOSC");
  	
  	
	close(fd_fifo_cs);
  	unlink("FIFOCS");*/

}
void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            //read(fd_fifo_cs, cad, sizeof(cad));
	    comCliente.Receive(cad, sizeof(cad));
            unsigned char key;
            int x, y;
            sscanf(cad,"%c %d %d", &key, &x, &y);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	
	struct mensaje m;
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		m.numpunt=puntos2;
		m.numjug= 2;
		write(fd_fifo, &m, sizeof(m));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		m.numpunt=puntos1;
		m.numjug= 1;
		write(fd_fifo, &m, sizeof(m));
	}
	
/*
//ESCRIBIR TUBERIA SERVIDOR-CLIENTE
	struct menSerCli msc;
	msc.e=esfera;
	msc.j1=jugador1;
	msc.j2=jugador2;
	msc.numpunt1=puntos1;
	msc.numpunt2=puntos2;
	write(fd_fifo_sc, &msc, sizeof(msc));
*/
	struct datossocket ds;
	ds.e=esfera;
	ds.j1=jugador1;
	ds.j2=jugador2;
	ds.numpunt1=puntos1;
	ds.numpunt2=puntos2;
	char datos[]={ds.e.centro.x, ds.e.centro.y, ds.j1.x1, ds.j1.y1, ds.j1.x2, ds.j1.y2, ds.j2.x1, ds.j2.y1, ds.j2.x2, ds.j2.y2, ds.numpunt1, ds.numpunt2};
	comCliente.Send(datos, sizeof(datos));
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
}
 

void CMundoServidor::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	
//FIFOS
	//TUBERIA LOGGER
	if ((fd_fifo=open("FIFO", O_WRONLY))<0) 
    {
     perror("No puede abrirse el FIFO");
     exit(1);
    }
    
/*
	 // abre el FIFO CLIENTE-SERVIDOR
    if ((fd_fifo_cs=open("FIFOCS", O_RDONLY))<0) 
    {
     perror("No puede abrirse el FIFO CLIENTE-SERVIDOR");
     exit(1);
    }

	 // abre FIFO SERVIDOR-CLIENTE
      if ((fd_fifo_sc=open("FIFOSC", O_WRONLY))<0) 
      {
       perror("No puede abrirse el FIFO SERVIDOR");
       exit(1);
      }
*/	
       
//THREAD

pthread_create(&pthread_id, 0, hilo_comandos, this);

//SOCKETS
conexion.InitInitServer("127.0.0.1",1500);
comCliente=conexion.Accept();
char nombre[100];
conexion.Receive(nombre, sizeof(nombre));
sscanf("Escriba el nombre del cliente:", &nombre);

}
