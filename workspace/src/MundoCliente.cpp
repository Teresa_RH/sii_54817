// MundoCliente.cpp: implementation of the CMundoCliente class.
//Editado por Teresa Ramirez Haro
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	/*close(fd_fifo_sc);
   	unlink("FIFOSC");
   	
   	close(fd_fifo_cs);
   	unlink("FIFOCS");*/

}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{
	/*struct menSerCli msc;
	read(fd_fifo_sc, &msc, sizeof(msc));
	esfera=msc.e;
	jugador1=msc.j1;
	jugador2=msc.j2;
	puntos1=msc.numpunt1;
	puntos2=msc.numpunt2;*/


//fprintf(stdout, "mensaje1\n");
	(DMC->esfera)=esfera;
	(DMC->raqueta1)=jugador1;
	if ((DMC->accion)==1) OnKeyboardDown('w',0,0);
	else {
		if ((DMC->accion)==-1) OnKeyboardDown('s',0 ,0);}

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char dato[10];
	sprintf(dato,"%c %d %d", key, x, y);
	write(fd_fifo_cs, &dato, sizeof(dato));	
	
}

void CMundoCliente::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
/*	
//FIFOS
	// crea el FIFO CLIENTE-SERVIDOR
	
// borra FIFOCS por si existía previamente
    unlink("FIFOCS");
 
    if ((mkfifo("FIFOCS", 0777))<0) 
    {
        perror("No puede crearse el FIFO CLIENTE-SERVIDOR");
    }
    
    
	// crea el FIFO SERVIDOR-CLIENTE
    
// borra FIFOSC por si existía previamente
   unlink("FIFOSC");
   
    if ((mkfifo("FIFOSC", 0777))<0) 
    {
        perror("No puede crearse el FIFO SERVIDOR-CLIENTE");
    }
    
    
    // abre el FIFO CLIENTE-SERVIDOR
    fd_fifo_cs=open("FIFOCS", O_WRONLY);
    if (fd_fifo_cs<0) 
    {
     perror("No puede abrirse el FIFO CLIENTE-SERVIDOR");
     exit(1);
    }

    // abre el FIFO SERVIDOR-CLIENTE
    fd_fifo_sc=open("FIFOSC", O_RDONLY);
     if (fd_fifo_sc<0) 
     {
      perror("No puede abrirse el FIFO SERVIDOR-CLIENTE");
      exit(1);
     }	
*/
	      
 //Mapeo memoria
        int b;
       if((b= open("BOTY", O_RDWR|O_CREAT|O_TRUNC,0777))<0)
       {
       perror("No puede abrirse el BOTY");
       }
       else
       {
    	write(b, &dt, sizeof(dt));
	DMC = (DatosMemCompartida*) mmap(0, sizeof(dt), PROT_READ|PROT_WRITE, MAP_SHARED, b,0);
	close(b);
	}

//SOCKETS
	char nombre[100];
	fprintf(stdr,"Nombre del cliente");
	sscanf(&nombre);
	comServidor.Send(nombre,sizeof(nombre));
 
        
}
