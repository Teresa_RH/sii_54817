// MundoCliente.h: interface for the CMundoCliente class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"

#include "Socket.h"

/*typedef struct menSerCli{
	Esfera e;
	Raqueta j1;
	Raqueta j2;
	unsigned int numpunt1;
	unsigned int numpunt2;
}menSerCli;*/



class CMundoCliente 
{
public:
	void Init();
	CMundoCliente();
	virtual ~CMundoCliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	
/*	
	//SERVIDOR-CLIENTE
	struct menSerCli msc;
	int fd_fifo_sc;
	
	//CLIENTE-SERVIDOR
	struct menSerCli mcs;
	int fd_fifo_cs;
*/

	//struct stat bstat;
	DatosMemCompartida * DMC;
	DatosMemCompartida dt;
	
	//SOCKETS
	Socket comServidor
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
