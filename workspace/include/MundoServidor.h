// MundoServidor.h: interface for the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"

#include <pthread.h>

#include "Socket.h"


/*typedef struct menSerCli{
	Esfera e;
	Raqueta j1;
	Raqueta j2;
	unsigned int numpunt1;
	unsigned int numpunt2;
}menSerCli;*/
typedef struct mensaje{
	unsigned int numpunt;
	unsigned int numjug;
}m;
typedef struct datossocket{
	Esfera e;
	Raqueta j1;
	Raqueta j2;
	unsigned int numpunt1;
	unsigned int numpunt2;
}datossocket;

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	
	struct mensaje m;
	int fd_fifo;
/*	
	//SERVIDOR-CLIENTE
	struct menSerCli msc;
	int fd_fifo_sc;
	
	//CLIENTE-SERVIDOR
	struct menSerCli mcs;
	int fd_fifo_cs;
*/
	
	//THREADS
	pthread_t pthread_id;
	void RecibeComandosJugador();
	
	//PROYECCION DE MEMORIA	
	DatosMemCompartida * DMC;
	DatosMemCompartida dt;
	
	//SOCKETS
	Socket conexion;
	Socket comCliente;
	struct datossocket ds;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
